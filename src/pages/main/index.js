import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

import baseURL from '../../services/api';
import './styles.css';

export default class Main extends Component {

  state = {
    products: [],
    productInfo: {},
    page: 1,
    error: false
  };

  componentDidMount() {
    this.loadData();
  }

  loadData = async (page = 1) => {
    try {
      const response = await axios.get(`/products?page=${page}`, { baseURL });
      const { docs, ...productInfo } = response.data;
  
      this.setState({ products: docs, productInfo, page: productInfo.page });
  
      if(window.pageYOffset || document.documentElement.scrollTop > 0) {
        window.scrollTo(0,0);
      }
    } catch(ex) {
      console.log('Error here. ',ex);
      this.setState({ error: true });
    }
  }

  prevPage = () => {
    const { page } = this.state;

    if(page === 1) return;
    
    const pageNumber = page - 1;

    this.loadData(pageNumber);
  };

  nextPage = () => {
    const { productInfo: { totalPages }, page } = this.state;
    
    if(page ===  totalPages) return;
    
    const pageNumber = page + 1;
    
    this.loadData(pageNumber);
  };

  render() {
    const { products, productInfo: { hasPrevPage, hasNextPage }, error } = this.state;
    
    if(error) {
      return <div className="error">Aconteceu um erro inesperado. Tente novamente mais tarde!</div>;
    }

    if(products.length === 0) {
      return <div className="loading">Loading...</div>;
    }

    return (
      <div className="product-list">
        {products.map(product => (
          <article key={product._id}>
            <strong>{product.title}</strong>
            <p>{product.description}</p>

            <Link to={`/products/${product._id}`}>Detalhes +</Link>
          </article>
        ))}

        <div className="paginate">
          <button disabled={!hasPrevPage} onClick={this.prevPage}>
            <span title={hasPrevPage ? 'Previous' : 'Disabled'} role="img" aria-label="Button prevPage">⇽</span>
          </button>

          <button disabled={!hasNextPage} onClick={this.nextPage}>
            <span title={hasNextPage ? 'Next' : 'Disabled'} role="img" aria-label="Button nextPage">⇾</span>
          </button>
        </div>
      </div>
    );
  }
}

