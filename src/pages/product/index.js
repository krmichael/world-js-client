import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

import baseURL from '../../services/api';
import './styles.css';

export default class Product extends Component {

  state = {
    product: {},
    error: false
  }

  async componentDidMount() {
    try {
      const { id } = this.props.match.params;
      
      const response = await axios.get(`/products/${id}`, { baseURL });
      
      this.setState({ product: response.data });
    } catch(ex) {
      console.log('Houve um erro, ',ex);
      this.setState({ error: true });
    }
  }

  render() {
    const { product, error } = this.state;

    if(error) {
      return <div className="error">Aconteceu um erro inesperado. Tente novamente mais tarde!</div>
    }

    return (
      <div className="container">
        <div className="product-info">
          <strong>{product.title}</strong>
          <p>{product.description}</p>

          <a href={product.url} target="_blank" rel="noopener noreferrer">Acesse: {product.url}</a>
        </div>

        <div className="back">
          <Link to="/">
            <span title="Back" role="img" aria-label="button back for home">⇽</span>
          </Link>
        </div>
      </div>
    );
  }
}

