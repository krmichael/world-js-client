import React from 'react';
import './styles.css';

const Header = () => (
  <header className="header">
    <h1>WorldJS <span role="img" aria-label="emation wink">😜</span></h1>
  </header>
);

export default Header;

